<!-- (c) https://github.com/MontiCore/monticore -->
[![Maintainability](https://api.codeclimate.com/v1/badges/d99a7593e52659f22d41/maintainability)](https://codeclimate.com/github/EmbeddedMontiArc/RewriteConf/maintainability)
[![Build Status](https://travis-ci.org/EmbeddedMontiArc/RewriteConf.svg?branch=master)](https://travis-ci.org/EmbeddedMontiArc/RewriteConf)
[![Build Status](https://circleci.com/gh/EmbeddedMontiArc/RewriteConf/tree/master.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/EmbeddedMontiArc/RewriteConf/tree/master)
[![Coverage Status](https://coveralls.io/repos/github/EmbeddedMontiArc/RewriteConf/badge.svg?branch=master)](https://coveralls.io/github/EmbeddedMontiArc/RewriteConf?branch=master)
# RewriteConf

A DSL extending OCL with C-like preprocessing features
