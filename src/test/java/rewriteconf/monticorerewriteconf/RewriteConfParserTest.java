/* (c) https://github.com/MontiCore/monticore */
package rewriteconf.monticorerewriteconf;


import rewriteconf.monticorerewriteconf.rewriteconf._ast.ASTCompilationUnit;
import de.se_rwth.commons.logging.Log;
import org.junit.*;
import rewriteconf.monticorerewriteconf.rewriteconf._parser.RewriteConfParser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.Assert.*;


public class RewriteConfParserTest {

    static boolean failQuick;

    @BeforeClass
    public static void startUp() {
        failQuick = Log.isFailQuickEnabled();
        Log.enableFailQuick(false);
    }

    @AfterClass
    public static void tearDown() {
        Log.enableFailQuick(failQuick);
    }

    @Before
    public void clear() {
        Log.getFindings().clear();
    }

    @Test
    public void test01() throws IOException {
        RewriteConfParser parser = new RewriteConfParser();

        ASTCompilationUnit ast = parser.parse("src/test/resources/conf/rewrite.conf").orElse(null);

        assertNotNull(ast);

        Log.debug(ast.getPackage().toString(), "package");
        Log.debug(ast.getImportStatements().toString(), "imports");
        Log.debug(ast.getRewriteRules().toString(), "rewrites");

    }


}
